const { Router } = require('express');

const router = Router();

router.get('/', ( req, res ) => {
    return res.status(200).json( {
        msg: 'Hola mundo - 201801434',
    } );
} );

router.post('/', ( req, res ) => {
    return res.status(200).json( {
        msg: 'Adios mundo - 201801434',
    } );
} );


module.exports = {
    router,
}
