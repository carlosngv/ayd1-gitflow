const express = require('express');
const cors = require('cors');

const { router } = require('../controllers/server.controller');

class Server {

    constructor() {
        this.app = express();
        this.port = 3000 | process.env.PORT
        this.middlewares();
        this.routes();
    }

    middlewares() {
        this.app.use( cors() );
        this.app.use( express.json() );
    }

    routes() {
        this.app.use( '', router );
    }

    listen() {
        this.app.listen( this.port, () => console.log(`Server listening on port ${ this.port }`))
    }

}

module.exports = Server;
